package kafka.producer.tasks;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.List;

public class SendMultipleTopicsTask extends Thread{

    KafkaProducer producer;
    List<ProducerRecord> records;

    public SendMultipleTopicsTask(KafkaProducer Producer, List<ProducerRecord> Records){
        this.producer = Producer;
        this.records = Records;
    }

    // Send Topic
    @Override
    public void run() {

        try {
            for(ProducerRecord record : records){
                producer.send(record);
            }
            return;
        } catch (Exception e) {
            throw e;
        }
    }
}
