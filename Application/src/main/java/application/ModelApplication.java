package application;

import kafka.consumer.ConsumerListener;
import kafka.consumer.KConsumer;
import kafka.producer.KProducer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import properties.EnvironmentVariables;

import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ModelApplication implements ConsumerListener {

    /**
     *  Logger
     */
    final Logger logger = LoggerFactory.getLogger(ModelApplication.class);

    private KProducer kProducer;

    private KConsumer kConsumer;

    /**
     *  ThreadPoll
     */
    private final ScheduledThreadPoolExecutor threadPool;

    private String serializeKey;
    private String deserializeKey;
    private String serializeValue;
    private String deserializeValue;

    private EnvironmentVariables environmentVariables;

    public static void main(String[] args) {

        new ModelApplication();
    }

    public ModelApplication(){

        serializeKey = StringSerializer.class.getName();
        deserializeKey = StringDeserializer.class.getName();
        //serializeValue = ByteArraySerializer.class.getName();
        //deserializeValue = ByteArrayDeserializer.class.getName();
        serializeValue = StringSerializer.class.getName();
        deserializeValue = StringDeserializer.class.getName();


        environmentVariables = new EnvironmentVariables();

        threadPool = new ScheduledThreadPoolExecutor(1000);

        try{
            logger.info("Starting Producer");

            kProducer = new KProducer(serializeKey, serializeValue, environmentVariables, "app.producer");

        }catch (Exception e){
            logger.error("Error starting producer", e);
            System.exit(1);
        }

        try {
            logger.info("Starting Consumer");

            kConsumer = new KConsumer(deserializeKey, deserializeValue, environmentVariables, "app.consumer");
            kConsumer.addConsumerListener(this);
            kConsumer.start();

        }catch (Exception e){
            logger.error("Error starting consumer", e);
            System.exit(1);
        }

    }

    @Override
    public void RecordReceived(ConsumerRecord Record) {
        logger.info("Record Received " + Record.value().toString());
    }
}
